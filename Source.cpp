#include <iostream>
#include <algorithm>
#include <vector>
#include <ctime>
#include <list>
using namespace std;

void main()
{
	int MaxCustomers, MinCustomers , MaxServiceTime, MinServiceTime, NumCashiers;
	int num_customers, arrived, timer, avg=0;
	vector<int> customers_arrived;
	vector<int> customers_time;
	vector<int> waittime;
	vector<int> lefttime;
	list<int> cashier;
	
	//Input value 
	cout << "Input MaxCustomers : ";
	cin >> MaxCustomers;

	cout << "Input MinCustomers : ";
	cin >> MinCustomers;

	cout << "Input MaxServiceTime : ";
	cin >> MaxServiceTime;

	cout << "Input MinServiceTime : ";
	cin >> MinServiceTime;

	cout << "Input NumCashiers : ";
	cin >> NumCashiers;

	srand(time(0));
	num_customers = rand()%((MaxCustomers+1)-MinCustomers) + MinCustomers; //random number of customers 
	
	for (int i = 0; i < num_customers; i++)
	{
		arrived = rand()%241 ; //random customers arrived time
		customers_arrived.push_back(arrived); //store value customers arrived time
	}
	sort(customers_arrived.begin(), customers_arrived.end()); //Sort the time for enqueue respective


	for (int i = 0; i < num_customers ; i++)
	{
		timer = rand() % ((MaxServiceTime+1) - MinServiceTime) + MinServiceTime; //random service time/customers
		customers_time.push_back(timer); //store value customers service time
	}

	int i = 0; //count customers 
	while (i < num_customers ) 
	{
		//To make wait time equal to 0 if each cashier service begin 
		while (i < NumCashiers) 
		{
			waittime.push_back(0);
			avg += waittime.at(i); //sum wait time 
			lefttime.push_back(customers_arrived.at(i) + customers_time.at(i)); //left time
			cashier.push_back(lefttime.at(i)); //store left time for sort
			cashier.sort(); //sort the left time to see what cashier is free
			i++; //next customers
		}

		//If left time minus customers arrived less than zero than mean customers not need to  wait the queue
		if (cashier.front() - customers_arrived.at(i) < 0)
		{
			waittime.push_back(0); 
			lefttime.push_back(customers_arrived.at(i) + customers_time.at(i)); //left time
			
		}
		//If left time minus customers arrived more than zero than mean customers need to wait the queue
		else
		{
			waittime.push_back(cashier.front() - customers_arrived.at(i)); //Make left minus arrive to find wait time
			lefttime.push_back(customers_arrived.at(i) + customers_time.at(i)+ waittime.at(i) );  //left time
			
		}
			cashier.push_back(lefttime.at(i)); //store left time for sort
			cashier.pop_front(); //Remove smallest value in list
			cashier.sort(); //sort the left time to see what cashier is free
			avg += waittime.at(i); //sum wait time 
		i++; //next customers
	}
	//Show status
	for (int i = 0; i <num_customers ; i++)
	{
		cout <<"Customer " << i + 1 << " --arrived " << customers_arrived.at(i)<<" --wait time "<<waittime.at(i)<<" --left "<< lefttime.at(i)  << endl;	
	}
	//Show average
	cout << "Average time - " << avg/num_customers << " minutes."<<endl;

system("Pause");
}
